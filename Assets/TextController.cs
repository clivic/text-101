﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour
{
	protected enum State
	{
		cell,
		sheets_0,
		mirror,
		lock_0,
		sheets_1,
		cell_mirror,
		lock_1,
		freedom
	}
	protected State currentState;
	public Text text;

	// Use this for initialization
	void Start ()
	{
		text.text = "Hello world!";
		currentState = State.cell;
	}
	
	// Update is called once per frame
	void Update ()
	{
		switch (currentState) {
		case State.cell:
			state_cell ();
			break;
		case State.sheets_0:
			state_sheets_0 ();
			break;
		case State.mirror:
			state_mirror ();
			break;
		case State.lock_0:
			state_lock_0 ();
			break;
		case State.sheets_1:
			state_sheets_1 ();
			break;
		case State.cell_mirror:
			state_cell_mirror ();
			break;
		case State.lock_1:
			state_lock_1 ();
			break;
		case State.freedom:
			state_freedom ();
			break;
		default:
			break;
		}
	}
	
	void state_cell ()
	{
		text.text = "You are in a prison cell, and you want to escape. " +
			"There are some dirty sheets on the bed, a mirror on the wall, " +
			"and the door is locked from the outside." +
			System.Environment.NewLine +
			System.Environment.NewLine + 
				
			"Press S to view Sheets, M to view Mirror and L to view Lock";
		
		if (Input.GetKeyUp (KeyCode.S)) {
			currentState = State.sheets_0;
		}
		if (Input.GetKeyUp (KeyCode.M)) {
			currentState = State.cell_mirror;
		}
		if (Input.GetKeyUp (KeyCode.L)) {
			currentState = State.lock_0;
		}
	}
	
	void state_sheets_0 ()
	{
		text.text = "You can't believe you sleep in these things. " +
			"Surely it's time somebody changed them." +
			" The pleasures of prison life I guess!" +
			System.Environment.NewLine +
			System.Environment.NewLine + 
			
			"Press R to Return to roaming your cell";
		if (Input.GetKeyUp (KeyCode.R)) {
			currentState = State.cell;
		}
	}
	
	void state_mirror ()
	{
		text.text = "The dirty old mirror on the wall seems loose." +
			System.Environment.NewLine +
			System.Environment.NewLine + 
			
			"Press T to Take the mirror, or R to Return to cell";
		if (Input.GetKeyUp (KeyCode.T)) {
			currentState = State.cell_mirror;
		}
		if (Input.GetKeyUp (KeyCode.R)) {
			currentState = State.cell;
		}
	}
	
	void state_lock_0 ()
	{
		text.text = "This is one of those button locks. " +
			"You have no idea what the combination is. " +
			"You wish you could somehow see where the dirty fingerprints were, " +
			"maybe that would help." +
			System.Environment.NewLine +
			System.Environment.NewLine + 
			
			"Press R to Return to roaming your cell";
		if (Input.GetKeyUp (KeyCode.R)) {		
			currentState = State.cell;
		}
	}
	
	void state_cell_mirror ()
	{
		text.text = "You are still in your cell, and you STILL want to escape! " +
			"There are some dirty sheets on the bed, a mark where the mirror was, " +
			"and that pesky door is still there, and firmly locked!" +
			System.Environment.NewLine +
			System.Environment.NewLine + 
			
			"Press S to view Sheets, or L to view Lock";
		if (Input.GetKeyUp (KeyCode.S)) {
			currentState = State.sheets_1;
		}
		if (Input.GetKeyUp (KeyCode.L)) {
			currentState = State.lock_1;
		}
	}
	
	void state_sheets_1 ()
	{
		text.text = "Holding a mirror in your hand doesn't make the sheets look any better." +
			System.Environment.NewLine +
			System.Environment.NewLine + 
			
			"Press R to Return to roaming your cell";
		if (Input.GetKeyDown (KeyCode.R)) {
			currentState = State.cell_mirror;
		}
	}
	
	void state_lock_1 ()
	{
		text.text = "You carefully put the mirror through the bars, " +
			"and turn it round so you can see the lock. " + 
			"You can just make out fingerprints around the buttons. " +
			"You press the dirty buttons, and hear a click." + 
			System.Environment.NewLine +
			System.Environment.NewLine + 
			
			"Press O to Open, or R to Return to your cell";
		if (Input.GetKeyDown (KeyCode.O)) {
			currentState = State.freedom;
		} else if (Input.GetKeyDown (KeyCode.R)) {
			currentState = State.cell_mirror;
		}
	}
	
	void state_freedom ()
	{
		text.text = "You are FREE!" +
			System.Environment.NewLine +
			System.Environment.NewLine + 
			
			"Press P to Play again";
		if (Input.GetKeyDown (KeyCode.P)) {
			currentState = State.cell;
		}
	}
}
